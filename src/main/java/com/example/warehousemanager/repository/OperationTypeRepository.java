package com.example.warehousemanager.repository;

import com.example.warehousemanager.model.entity.OperationTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationTypeRepository extends JpaRepository<OperationTypeEntity, Long> {
}
