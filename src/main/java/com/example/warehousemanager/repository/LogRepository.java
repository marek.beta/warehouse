package com.example.warehousemanager.repository;

import com.example.warehousemanager.model.entity.LogEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface LogRepository extends JpaRepository<LogEntity, Long> {
    Page<LogEntity> findByTimestampBetween(LocalDateTime fromDateTime, LocalDateTime toDateTime, Pageable pageable);

    Page<LogEntity> findByTimestampBetweenAndProduct_Ean(LocalDateTime fromDateTime, LocalDateTime toDateTime, String productEan, Pageable pageable);
}
