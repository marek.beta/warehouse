package com.example.warehousemanager.repository;

import com.example.warehousemanager.model.entity.SubWarehouseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubWarehouseRepository extends JpaRepository<SubWarehouseEntity, Long> {
}
