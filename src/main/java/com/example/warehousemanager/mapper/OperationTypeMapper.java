package com.example.warehousemanager.mapper;

import com.example.warehousemanager.model.dto.OperationTypeDto;
import com.example.warehousemanager.model.dto.OperationTypeResponseDto;
import com.example.warehousemanager.model.entity.OperationTypeEntity;
import org.springframework.stereotype.Service;

@Service
public class OperationTypeMapper {
    public OperationTypeResponseDto toResponseDto(OperationTypeEntity operationTypeEntity) {
        return OperationTypeResponseDto.builder()
                .id(operationTypeEntity.getId())
                .name(operationTypeEntity.getName())
                .build();
    }

    public OperationTypeDto toDto(OperationTypeEntity operationTypeEntity) {
        return OperationTypeDto.builder()
                .name(operationTypeEntity.getName())
                .build();
    }

    public OperationTypeEntity toEntity(OperationTypeResponseDto operationTypeDto) {
        return OperationTypeEntity.builder()
                .id(operationTypeDto.getId())
                .name(operationTypeDto.getName())
                .build();
    }

    public OperationTypeEntity toEntity(OperationTypeDto operationTypeDto) {
        return OperationTypeEntity.builder()
                .name(operationTypeDto.getName())
                .build();
    }
}
