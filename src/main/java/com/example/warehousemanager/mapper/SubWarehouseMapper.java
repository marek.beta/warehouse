package com.example.warehousemanager.mapper;

import com.example.warehousemanager.model.dto.SubWarehouseDto;
import com.example.warehousemanager.model.dto.SubWarehouseResponseDto;
import com.example.warehousemanager.model.entity.SubWarehouseEntity;
import com.example.warehousemanager.repository.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SubWarehouseMapper {
    private final WarehouseRepository warehouseRepository;

    public SubWarehouseResponseDto toResponseDto(SubWarehouseEntity subWarehouseEntity) {
        return SubWarehouseResponseDto.builder()
                .id(subWarehouseEntity.getId())
                .name(subWarehouseEntity.getName())
                .warehouseId(subWarehouseEntity.getWarehouse().getId())
                .address(subWarehouseEntity.getAddress())
                .build();
    }

    public SubWarehouseDto toDto(SubWarehouseEntity subWarehouseEntity) {
        return SubWarehouseDto.builder()
                .warehouseId(subWarehouseEntity.getWarehouse().getId())
                .name(subWarehouseEntity.getName())
                .address(subWarehouseEntity.getAddress())
                .build();
    }

    public SubWarehouseEntity toEntity(SubWarehouseResponseDto subWarehouseResponseDto) {
        return SubWarehouseEntity.builder()
                .id(subWarehouseResponseDto.getId())
                .name(subWarehouseResponseDto.getName())
                .warehouse(warehouseRepository.findById(subWarehouseResponseDto.getWarehouseId()).orElseThrow())
                .address(subWarehouseResponseDto.getAddress())
                .build();
    }

    public SubWarehouseEntity toEntity(SubWarehouseDto subWarehouseDto) {
        return SubWarehouseEntity.builder()
                .name(subWarehouseDto.getName())
                .warehouse(warehouseRepository.findById(subWarehouseDto.getWarehouseId()).orElseThrow())
                .address(subWarehouseDto.getAddress())
                .build();
    }
}
