package com.example.warehousemanager.mapper;

import com.example.warehousemanager.model.dto.LogDto;
import com.example.warehousemanager.model.dto.LogResponseDto;
import com.example.warehousemanager.model.entity.LogEntity;
import com.example.warehousemanager.repository.ProductRepository;
import com.example.warehousemanager.repository.SubWarehouseRepository;
import com.example.warehousemanager.repository.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LogMapper {
    private final ProductRepository productRepository;
    private final SubWarehouseRepository subWarehouseRepository;
    private final WarehouseRepository warehouseRepository;

    public LogResponseDto toResponseDto(LogEntity logEntity) {
        return LogResponseDto
                .builder()
                .id(logEntity.getId())
                .quantity(logEntity.getQuantity())
                .productEan(logEntity.getProduct().getEan())
                .warehouseFromId(logEntity.getWarehouseFrom().getId())
                .warehouseToId(logEntity.getWarehouseTo().getId())
                .subWarehouseFromId(logEntity.getSubWarehouseFrom() != null ? logEntity.getSubWarehouseFrom().getId() : null)
                .subWarehouseToId(logEntity.getSubWarehouseTo() != null ? logEntity.getSubWarehouseTo().getId() : null)
                .timestamp(logEntity.getTimestamp())
                .build();
    }

    public LogDto toDto(LogEntity logEntity) {
        return LogDto
                .builder()
                .quantity(logEntity.getQuantity())
                .productEan(logEntity.getProduct().getEan())
                .warehouseFromId(logEntity.getWarehouseFrom().getId())
                .warehouseToId(logEntity.getWarehouseTo().getId())
                .subWarehouseFromId(logEntity.getSubWarehouseFrom() != null ? logEntity.getSubWarehouseFrom().getId() : null)
                .subWarehouseToId(logEntity.getSubWarehouseTo() != null ? logEntity.getSubWarehouseTo().getId() : null)
                .build();
    }

    public LogEntity toEntity(LogResponseDto logResponseDto) {
        return LogEntity
                .builder()
                .id(logResponseDto.getId())
                .quantity(logResponseDto.getQuantity())
                .product(productRepository.findById(logResponseDto.getProductEan()).orElseThrow())
                .warehouseFrom(warehouseRepository.findById(logResponseDto.getWarehouseFromId()).orElseThrow())
                .warehouseTo(warehouseRepository.findById(logResponseDto.getWarehouseToId()).orElseThrow())
                .subWarehouseFrom(logResponseDto.getSubWarehouseFromId() != null ? subWarehouseRepository.findById(logResponseDto.getSubWarehouseFromId()).orElseThrow() : null)
                .subWarehouseTo(logResponseDto.getSubWarehouseToId() != null ? subWarehouseRepository.findById(logResponseDto.getSubWarehouseToId()).orElseThrow() : null)
                .timestamp(logResponseDto.getTimestamp())
                .build();
    }

    public LogEntity toEntity(LogDto logDto) {
        return LogEntity
                .builder()
                .quantity(logDto.getQuantity())
                .product(productRepository.findById(logDto.getProductEan()).orElseThrow())
                .warehouseFrom(warehouseRepository.findById(logDto.getWarehouseFromId()).orElseThrow())
                .warehouseTo(warehouseRepository.findById(logDto.getWarehouseToId()).orElseThrow())
                .subWarehouseFrom(logDto.getSubWarehouseFromId() != null ? subWarehouseRepository.findById(logDto.getSubWarehouseFromId()).orElseThrow() : null)
                .subWarehouseTo(logDto.getSubWarehouseToId() != null ? subWarehouseRepository.findById(logDto.getSubWarehouseToId()).orElseThrow() : null)
                .build();
    }
}
