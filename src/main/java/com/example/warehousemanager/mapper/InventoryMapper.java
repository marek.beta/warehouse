package com.example.warehousemanager.mapper;

import com.example.warehousemanager.model.dto.InventoryDto;
import com.example.warehousemanager.model.dto.InventoryResponseDto;
import com.example.warehousemanager.model.entity.InventoryEntity;
import com.example.warehousemanager.repository.OperationTypeRepository;
import com.example.warehousemanager.repository.ProductRepository;
import com.example.warehousemanager.repository.SubWarehouseRepository;
import com.example.warehousemanager.repository.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InventoryMapper {
    private final ProductRepository productRepository;
    private final WarehouseRepository warehouseRepository;
    private final SubWarehouseRepository subwarehouseRepository;
    private final OperationTypeRepository operationTypeRepository;

    public InventoryResponseDto toResponseDto(InventoryEntity inventoryEntity) {
        return InventoryResponseDto.builder()
                .id(inventoryEntity.getId())
                .productEan(inventoryEntity.getProduct().getEan())
                .operationTypeId(inventoryEntity.getOperationType().getId())
                .warehouseId(inventoryEntity.getWarehouse().getId())
                .subWarehouseId(inventoryEntity.getSubWarehouse() == null ? null : inventoryEntity.getSubWarehouse().getId())
                .serialNumber(inventoryEntity.getSerialNumber())
                .expirationDate(inventoryEntity.getExpirationDate())
                .quantity(inventoryEntity.getQuantity())
                .build();
    }

    public InventoryDto toDto(InventoryEntity inventoryEntity) {
        return InventoryDto.builder()
                .productEan(inventoryEntity.getProduct().getEan())
                .operationTypeId(inventoryEntity.getOperationType().getId())
                .warehouseId(inventoryEntity.getWarehouse().getId())
                .subWarehouseId(inventoryEntity.getSubWarehouse() == null ? null : inventoryEntity.getSubWarehouse().getId())
                .serialNumber(inventoryEntity.getSerialNumber())
                .expirationDate(inventoryEntity.getExpirationDate())
                .quantity(inventoryEntity.getQuantity())
                .build();
    }

    public InventoryEntity toEntity(InventoryResponseDto inventoryResponseDto) {
        return InventoryEntity.builder()
                .id(inventoryResponseDto.getId())
                .product(productRepository.findById(inventoryResponseDto.getProductEan()).orElseThrow())
                .warehouse(warehouseRepository.findById(inventoryResponseDto.getWarehouseId()).orElseThrow())
                .subWarehouse(inventoryResponseDto.getSubWarehouseId() == null ? null : subwarehouseRepository.findById(inventoryResponseDto.getSubWarehouseId()).orElseThrow())
                .operationType(operationTypeRepository.findById(inventoryResponseDto.getOperationTypeId()).orElseThrow())
                .serialNumber(inventoryResponseDto.getSerialNumber())
                .expirationDate(inventoryResponseDto.getExpirationDate())
                .quantity(inventoryResponseDto.getQuantity())
                .build();
    }

    public InventoryEntity toEntity(InventoryDto inventoryDto) {
        return InventoryEntity.builder()
                .product(productRepository.findById(inventoryDto.getProductEan()).orElseThrow())
                .warehouse(warehouseRepository.findById(inventoryDto.getWarehouseId()).orElseThrow())
                .subWarehouse(inventoryDto.getSubWarehouseId() == null ? null : subwarehouseRepository.findById(inventoryDto.getSubWarehouseId()).orElseThrow())
                .operationType(operationTypeRepository.findById(inventoryDto.getOperationTypeId()).orElseThrow())
                .serialNumber(inventoryDto.getSerialNumber())
                .quantity(inventoryDto.getQuantity())
                .expirationDate(inventoryDto.getExpirationDate())
                .build();
    }
}
