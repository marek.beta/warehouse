package com.example.warehousemanager.mapper;

import com.example.warehousemanager.model.dto.ProductDto;
import com.example.warehousemanager.model.entity.ProductEntity;
import org.springframework.stereotype.Service;

@Service
public class ProductMapper {
    public ProductDto toResponseDto(ProductEntity productEntity) {
        return ProductDto.builder()
                .ean(productEntity.getEan())
                .name(productEntity.getName())
                .build();
    }

    public ProductEntity toEntity(ProductDto productDto) {
        return ProductEntity.builder()
                .ean(productDto.getEan())
                .name(productDto.getName())
                .build();
    }
}
