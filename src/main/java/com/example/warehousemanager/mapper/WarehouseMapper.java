package com.example.warehousemanager.mapper;

import com.example.warehousemanager.model.dto.WarehouseDto;
import com.example.warehousemanager.model.dto.WarehouseResponseDto;
import com.example.warehousemanager.model.entity.WarehouseEntity;
import org.springframework.stereotype.Service;

@Service
public class WarehouseMapper {
    public WarehouseResponseDto toResponseDto(WarehouseEntity warehouseEntity) {
        return WarehouseResponseDto.builder()
                .id(warehouseEntity.getId())
                .name(warehouseEntity.getName())
                .address(warehouseEntity.getAddress())
                .build();
    }

    public WarehouseDto toDto(WarehouseEntity warehouseEntity) {
        return WarehouseDto.builder()
                .name(warehouseEntity.getName())
                .address(warehouseEntity.getAddress())
                .build();
    }

    public WarehouseEntity toEntity(WarehouseResponseDto warehouseResponseDto) {
        return WarehouseEntity.builder()
                .id(warehouseResponseDto.getId())
                .name(warehouseResponseDto.getName())
                .address(warehouseResponseDto.getAddress())
                .build();
    }

    public WarehouseEntity toEntity(WarehouseDto warehouseDto) {
        return WarehouseEntity.builder()
                .name(warehouseDto.getName())
                .address(warehouseDto.getAddress())
                .build();
    }
}
