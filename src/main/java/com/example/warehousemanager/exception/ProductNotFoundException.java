package com.example.warehousemanager.exception;

import org.springframework.http.HttpStatus;

public class ProductNotFoundException extends Exception {
    private String code;
    private HttpStatus httpStatus;

    public ProductNotFoundException(String id) {
        super("Product with ean " + id + " not found");
    }
}
