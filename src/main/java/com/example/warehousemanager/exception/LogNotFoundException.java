package com.example.warehousemanager.exception;

import org.springframework.http.HttpStatus;

public class LogNotFoundException extends Exception {
    private String code;
    private HttpStatus httpStatus;

    public LogNotFoundException(String id) {
        super("Log with id " + id + " not found");
    }
}
