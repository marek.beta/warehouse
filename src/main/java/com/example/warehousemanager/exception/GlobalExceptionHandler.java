package com.example.warehousemanager.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = InventoryNotFoundException.class)
    public ResponseEntity<ApiError> inventoryNotFoundExceptionHandler(InventoryNotFoundException e) {
        ApiError apiError = new ApiError(e.getMessage(), "A1");

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = LogNotFoundException.class)
    public ResponseEntity<ApiError> logNotFoundExceptionHandler(LogNotFoundException e) {
        ApiError apiError = new ApiError(e.getMessage(), "A2");

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OperationTypeNotFoundException.class)
    public ResponseEntity<ApiError> operationTypeNotFoundExceptionHandler(OperationTypeNotFoundException e) {
        ApiError apiError = new ApiError(e.getMessage(), "A3");

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ProductNotFoundException.class)
    public ResponseEntity<ApiError> productNotFoundExceptionHandler(ProductNotFoundException e) {
        ApiError apiError = new ApiError(e.getMessage(), "A4");

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = SubWarehouseNotFoundException.class)
    public ResponseEntity<ApiError> subWarehouseNotFoundExceptionHandler(SubWarehouseNotFoundException e) {
        ApiError apiError = new ApiError(e.getMessage(), "A5");

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = WarehouseNotFoundException.class)
    public ResponseEntity<ApiError> warehouseNotFoundExceptionHandler(WarehouseNotFoundException e) {
        ApiError apiError = new ApiError(e.getMessage(), "A6");

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ApiError> exceptionHandler(Exception e) {
        ApiError apiError = new ApiError(e.getMessage(), "A7");

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }
}
