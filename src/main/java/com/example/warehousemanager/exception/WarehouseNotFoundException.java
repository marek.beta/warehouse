package com.example.warehousemanager.exception;

import org.springframework.http.HttpStatus;

public class WarehouseNotFoundException extends Exception {
    private String code;
    private HttpStatus httpStatus;

    public WarehouseNotFoundException(String id) {
        super("Warehouse with id " + id + " not found");
    }
}
