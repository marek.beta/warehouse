package com.example.warehousemanager.exception;

import org.springframework.http.HttpStatus;

public class OperationTypeNotFoundException extends Exception {
    private String code;
    private HttpStatus httpStatus;

    public OperationTypeNotFoundException(String id) {
        super("OperationType with id " + id + " not found");
    }
}
