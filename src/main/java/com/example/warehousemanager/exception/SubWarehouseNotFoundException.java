package com.example.warehousemanager.exception;

import org.springframework.http.HttpStatus;

public class SubWarehouseNotFoundException extends Exception {
    private String code;
    private HttpStatus httpStatus;

    public SubWarehouseNotFoundException(String id) {
        super("SubWarehouse with id " + id + " not found");
    }
}
