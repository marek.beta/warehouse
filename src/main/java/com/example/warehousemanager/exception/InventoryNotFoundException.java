package com.example.warehousemanager.exception;

import org.springframework.http.HttpStatus;

public class InventoryNotFoundException extends Exception {
    private String code;
    private HttpStatus httpStatus;

    public InventoryNotFoundException(String id) {
        super("Inventory with id " + id + " not found");
    }
}
