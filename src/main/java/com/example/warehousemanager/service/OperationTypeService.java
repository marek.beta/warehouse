package com.example.warehousemanager.service;

import com.example.warehousemanager.exception.OperationTypeNotFoundException;
import com.example.warehousemanager.mapper.OperationTypeMapper;
import com.example.warehousemanager.model.dto.OperationTypeDto;
import com.example.warehousemanager.model.dto.OperationTypeResponseDto;
import com.example.warehousemanager.model.entity.OperationTypeEntity;
import com.example.warehousemanager.repository.OperationTypeRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OperationTypeService {
    private final OperationTypeRepository operationTypeRepository;
    private final OperationTypeMapper operationTypeMapper;
    private final ObjectMapper objectMapper;

    public Page<OperationTypeResponseDto> findAll(Pageable pageable) {
        return operationTypeRepository
                .findAll(pageable)
                .map(operationTypeMapper::toResponseDto);
    }

    public OperationTypeResponseDto findOperationTypeById(Long id) throws OperationTypeNotFoundException {
        OperationTypeEntity operationTypeEntity = operationTypeRepository
                .findById(id)
                .orElseThrow(() -> new OperationTypeNotFoundException(id.toString()));

        return operationTypeMapper.toResponseDto(operationTypeEntity);
    }

    public OperationTypeResponseDto createOperationType(OperationTypeDto operationTypeDto) {
        OperationTypeEntity operationTypeEntity = operationTypeMapper.toEntity(operationTypeDto);
        OperationTypeEntity savedOperationTypeEntity = operationTypeRepository.saveAndFlush(operationTypeEntity);

        return operationTypeMapper.toResponseDto(savedOperationTypeEntity);
    }

    public OperationTypeResponseDto updateOperationType(Long id, JsonMergePatch patch) throws OperationTypeNotFoundException {
        OperationTypeResponseDto targetOperationType = findOperationTypeById(id);
        OperationTypeResponseDto patchedOperationType = applyPatchToOperationType(patch, targetOperationType);

        OperationTypeEntity operationTypeEntity = operationTypeMapper.toEntity(patchedOperationType);
        OperationTypeEntity savedOperationType = operationTypeRepository.save(operationTypeEntity);

        return operationTypeMapper.toResponseDto(savedOperationType);
    }

    public void deleteOperationTypeById(Long id) {
        operationTypeRepository.deleteById(id);
    }

    private OperationTypeResponseDto applyPatchToOperationType(JsonMergePatch patch, OperationTypeResponseDto targetOperationType) {
        JsonNode productNode = objectMapper.valueToTree(targetOperationType);
        JsonNode patchedOperationType;

        try {
            patchedOperationType = patch.apply(productNode);
        } catch (JsonPatchException e) {
            throw new IllegalArgumentException("Invalid patch");
        }

        return objectMapper.convertValue(patchedOperationType, OperationTypeResponseDto.class);
    }
}
