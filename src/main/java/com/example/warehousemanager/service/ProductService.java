package com.example.warehousemanager.service;

import com.example.warehousemanager.exception.ProductNotFoundException;
import com.example.warehousemanager.mapper.ProductMapper;
import com.example.warehousemanager.model.dto.ProductDto;
import com.example.warehousemanager.model.entity.ProductEntity;
import com.example.warehousemanager.repository.ProductRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final ObjectMapper objectMapper;

    public Page<ProductDto> findAll(Pageable pageable) {
        return productRepository
                .findAll(pageable)
                .map(productMapper::toResponseDto);
    }

    public ProductDto findProductByEan(String ean) throws ProductNotFoundException {
        ProductEntity productEntity = productRepository
                .findById(ean)
                .orElseThrow(() -> new ProductNotFoundException(ean));

        return productMapper.toResponseDto(productEntity);
    }

    public ProductDto createProduct(ProductDto product) {
        ProductEntity productEntity = productMapper.toEntity(product);
        ProductEntity savedProductEntity = productRepository.save(productEntity);

        return productMapper.toResponseDto(savedProductEntity);
    }

    public ProductDto updateProduct(String ean, JsonMergePatch patch) throws ProductNotFoundException {
        ProductDto targetProduct = findProductByEan(ean);
        ProductDto patchedProduct = applyPatchToProduct(patch, targetProduct);

        ProductEntity productEntity = productMapper.toEntity(patchedProduct);
        ProductEntity savedProduct = productRepository.save(productEntity);

        return productMapper.toResponseDto(savedProduct);
    }

    public void deleteProductByEan(String ean) {
        productRepository.deleteById(ean);
    }

    private ProductDto applyPatchToProduct(JsonMergePatch patch, ProductDto targetProduct) {
        JsonNode productNode = objectMapper.valueToTree(targetProduct);
        JsonNode patchedProduct;

        try {
            patchedProduct = patch.apply(productNode);
        } catch (JsonPatchException e) {
            throw new IllegalArgumentException("Invalid patch");
        }

        return objectMapper.convertValue(patchedProduct, ProductDto.class);
    }
}
