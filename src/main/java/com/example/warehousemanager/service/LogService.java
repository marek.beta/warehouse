package com.example.warehousemanager.service;

import com.example.warehousemanager.exception.LogNotFoundException;
import com.example.warehousemanager.mapper.LogMapper;
import com.example.warehousemanager.model.dto.LogDto;
import com.example.warehousemanager.model.dto.LogResponseDto;
import com.example.warehousemanager.model.entity.LogEntity;
import com.example.warehousemanager.repository.LogRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class LogService {
    private final LogRepository logRepository;
    private final LogMapper logMapper;

    private final ObjectMapper objectMapper;

    public Page<LogResponseDto> findAll(Pageable pageable) {
        return logRepository
                .findAll(pageable)
                .map(logMapper::toResponseDto);
    }

    public LogResponseDto findLogById(Long id) throws LogNotFoundException {
        LogEntity logEntity = logRepository
                .findById(id)
                .orElseThrow(() -> new LogNotFoundException(id.toString()));

        return logMapper.toResponseDto(logEntity);
    }

    public LogResponseDto createLog(LogDto logDto) {
        LogEntity logEntity = logMapper.toEntity(logDto);
        LogEntity savedLogEntity = logRepository.saveAndFlush(logEntity);

        return logMapper.toResponseDto(savedLogEntity);
    }

    public LogResponseDto updateLog(Long id, JsonMergePatch logPatch) throws LogNotFoundException {
        LogResponseDto targetLog = findLogById(id);
        LogResponseDto patchedLog = applyPatchToLog(logPatch, targetLog);

        LogEntity logEntity = logMapper.toEntity(patchedLog);
        LogEntity savedLog = logRepository.save(logEntity);

        return logMapper.toResponseDto(savedLog);
    }

    public void deleteLogById(Long id) {
        logRepository.deleteById(id);
    }

    public Page<LogResponseDto> searchLogs(LocalDateTime fromDateTime, LocalDateTime toDateTime, Pageable pageable) {
        return logRepository
                .findByTimestampBetween(fromDateTime, toDateTime, pageable)
                .map(logMapper::toResponseDto);
    }

    public Page<LogResponseDto> searchLogs(String productEan, LocalDateTime fromDateTime, LocalDateTime toDateTime, Pageable pageable) {
        return logRepository
                .findByTimestampBetweenAndProduct_Ean(fromDateTime, toDateTime, productEan, pageable)
                .map(logMapper::toResponseDto);
    }


    private LogResponseDto applyPatchToLog(JsonMergePatch patch, LogResponseDto targetLog) {
        JsonNode logNode = objectMapper.valueToTree(targetLog);
        JsonNode patchedLog;

        try {
            patchedLog = patch.apply(logNode);
        } catch (JsonPatchException e) {
            throw new IllegalArgumentException("Invalid patch");
        }

        return objectMapper.convertValue(patchedLog, LogResponseDto.class);
    }
}
