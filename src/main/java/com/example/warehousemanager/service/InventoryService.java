package com.example.warehousemanager.service;

import com.example.warehousemanager.exception.InventoryNotFoundException;
import com.example.warehousemanager.mapper.InventoryMapper;
import com.example.warehousemanager.model.dto.InventoryDto;
import com.example.warehousemanager.model.dto.InventoryResponseDto;
import com.example.warehousemanager.model.entity.InventoryEntity;
import com.example.warehousemanager.repository.InventoryRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InventoryService {
    private final InventoryRepository inventoryRepository;
    private final InventoryMapper inventoryMapper;

    private final ObjectMapper objectMapper;

    public Page<InventoryResponseDto> findAll(Pageable pageable) {
        return inventoryRepository
                .findAll(pageable)
                .map(inventoryMapper::toResponseDto);
    }

    public InventoryResponseDto findInventoryById(Long id) throws InventoryNotFoundException {
        InventoryEntity inventoryEntity = inventoryRepository
                .findById(id)
                .orElseThrow(() -> new InventoryNotFoundException(id.toString()));

        return inventoryMapper.toResponseDto(inventoryEntity);
    }

    public InventoryResponseDto createInventory(InventoryDto inventoryDto) {
        InventoryEntity inventoryEntity = inventoryMapper.toEntity(inventoryDto);
        InventoryEntity savedInventoryEntity = inventoryRepository.saveAndFlush(inventoryEntity);

        return inventoryMapper.toResponseDto(savedInventoryEntity);
    }

    public InventoryResponseDto updateInventory(Long id, JsonMergePatch patch) throws InventoryNotFoundException {
        InventoryResponseDto targetInventory = findInventoryById(id);
        InventoryResponseDto patchedInventory = applyPatchToInventory(patch, targetInventory);

        InventoryEntity inventoryEntity = inventoryMapper.toEntity(patchedInventory);
        InventoryEntity savedInventoryEntity = inventoryRepository.save(inventoryEntity);

        return inventoryMapper.toResponseDto(savedInventoryEntity);
    }

    public void deleteInventory(Long id) {
        inventoryRepository.deleteById(id);
    }

    private InventoryResponseDto applyPatchToInventory(JsonMergePatch patch, InventoryResponseDto targetInventory) {
        JsonNode inventoryNode = objectMapper.valueToTree(targetInventory);
        JsonNode patchedInventory;

        try {
            patchedInventory = patch.apply(inventoryNode);
        } catch (JsonPatchException e) {
            throw new IllegalArgumentException("Invalid patch");
        }

        return objectMapper.convertValue(patchedInventory, InventoryResponseDto.class);
    }
}
