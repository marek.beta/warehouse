package com.example.warehousemanager.service;

import com.example.warehousemanager.exception.WarehouseNotFoundException;
import com.example.warehousemanager.mapper.WarehouseMapper;
import com.example.warehousemanager.model.dto.WarehouseDto;
import com.example.warehousemanager.model.dto.WarehouseResponseDto;
import com.example.warehousemanager.model.entity.SubWarehouseEntity;
import com.example.warehousemanager.model.entity.WarehouseEntity;
import com.example.warehousemanager.repository.SubWarehouseRepository;
import com.example.warehousemanager.repository.WarehouseRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WarehouseService {
    private final WarehouseRepository warehouseRepository;
    private final SubWarehouseRepository subWarehouseRepository;
    private final WarehouseMapper warehouseMapper;
    private final ObjectMapper objectMapper;

    public Page<WarehouseResponseDto> findAll(Pageable pageable) {
        return warehouseRepository
                .findAll(pageable)
                .map(warehouseMapper::toResponseDto);
    }

    public WarehouseResponseDto findWarehouseById(Long id) throws WarehouseNotFoundException {
        WarehouseEntity warehouseEntity = warehouseRepository
                .findById(id)
                .orElseThrow(() -> new WarehouseNotFoundException(id.toString()));

        return warehouseMapper.toResponseDto(warehouseEntity);
    }

    public WarehouseResponseDto createWarehouse(WarehouseDto warehouseDto) {
        WarehouseEntity warehouseEntity = warehouseMapper.toEntity(warehouseDto);
        WarehouseEntity savedWarehouse = warehouseRepository.saveAndFlush(warehouseEntity);

        return warehouseMapper.toResponseDto(savedWarehouse);
    }

    public WarehouseResponseDto updateWarehouse(Long id, JsonMergePatch patch) throws WarehouseNotFoundException {
        WarehouseResponseDto targetWarehouse = findWarehouseById(id);
        WarehouseResponseDto patchedWarehouse = applyPatchToWarehouse(patch, targetWarehouse);

        WarehouseEntity warehouseEntity = warehouseMapper.toEntity(patchedWarehouse);
        WarehouseEntity savedWarehouse = warehouseRepository.save(warehouseEntity);

        return warehouseMapper.toResponseDto(savedWarehouse);
    }

    public void deleteWarehouseById(Long id) {
        warehouseRepository.deleteById(id);
    }


    public WarehouseResponseDto attachSubWarehouseToWarehouse(Long warehouseId, Long subWarehouseId) {
        WarehouseEntity warehouseEntity = warehouseRepository.findById(warehouseId).orElseThrow();
        SubWarehouseEntity subWarehouseEntity = subWarehouseRepository.findById(subWarehouseId).orElseThrow();

        warehouseEntity.addSubWarehouse(subWarehouseEntity);
        WarehouseEntity savedWarehouse = warehouseRepository.save(warehouseEntity);

        return warehouseMapper.toResponseDto(savedWarehouse);
    }

    private WarehouseResponseDto applyPatchToWarehouse(JsonMergePatch patch, WarehouseResponseDto targetWarehouse) {
        JsonNode warehouseNode = objectMapper.valueToTree(targetWarehouse);
        JsonNode patchedWarehouse;

        try {
            patchedWarehouse = patch.apply(warehouseNode);
        } catch (JsonPatchException e) {
            throw new IllegalArgumentException("Invalid patch");
        }

        return objectMapper.convertValue(patchedWarehouse, WarehouseResponseDto.class);
    }
}
