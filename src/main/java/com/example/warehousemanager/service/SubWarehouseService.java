package com.example.warehousemanager.service;

import com.example.warehousemanager.exception.SubWarehouseNotFoundException;
import com.example.warehousemanager.mapper.SubWarehouseMapper;
import com.example.warehousemanager.model.dto.SubWarehouseDto;
import com.example.warehousemanager.model.dto.SubWarehouseResponseDto;
import com.example.warehousemanager.model.entity.SubWarehouseEntity;
import com.example.warehousemanager.repository.SubWarehouseRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SubWarehouseService {
    private final SubWarehouseRepository subWarehouseRepository;
    private final SubWarehouseMapper subWarehouseMapper;

    private final ObjectMapper objectMapper;

    public Page<SubWarehouseResponseDto> findAll(Pageable pageable) {
        return subWarehouseRepository
                .findAll(pageable)
                .map(subWarehouseMapper::toResponseDto);
    }

    public SubWarehouseResponseDto findSubWarehouseById(Long id) throws SubWarehouseNotFoundException {
        SubWarehouseEntity subWarehouseEntity = subWarehouseRepository
                .findById(id)
                .orElseThrow(() -> new SubWarehouseNotFoundException(id.toString()));

        return subWarehouseMapper.toResponseDto(subWarehouseEntity);
    }

    public SubWarehouseResponseDto createSubWarehouse(SubWarehouseDto subWarehouseDto) {
        SubWarehouseEntity subWarehouseEntity = subWarehouseMapper.toEntity(subWarehouseDto);
        SubWarehouseEntity savedSubWarehouseEntity = subWarehouseRepository.saveAndFlush(subWarehouseEntity);

        return subWarehouseMapper.toResponseDto(savedSubWarehouseEntity);
    }

    public SubWarehouseResponseDto updateSubWarehouse(Long id, JsonMergePatch patch) throws SubWarehouseNotFoundException {
        SubWarehouseResponseDto targetSubWarehouse = findSubWarehouseById(id);
        SubWarehouseResponseDto patchedSubWarehouse = applyPatchToProduct(patch, targetSubWarehouse);

        SubWarehouseEntity subWarehouseEntity = subWarehouseMapper.toEntity(patchedSubWarehouse);
        SubWarehouseEntity savedSubWarehouse = subWarehouseRepository.save(subWarehouseEntity);

        return subWarehouseMapper.toResponseDto(savedSubWarehouse);
    }

    public void deleteSubWarehouseById(Long id) {
        subWarehouseRepository.deleteById(id);
    }

    private SubWarehouseResponseDto applyPatchToProduct(JsonMergePatch patch, SubWarehouseResponseDto targetSubWarehouse) {
        JsonNode subWarehouseNode = objectMapper.valueToTree(targetSubWarehouse);
        JsonNode patchedSubWarehouse;

        try {
            patchedSubWarehouse = patch.apply(subWarehouseNode);
        } catch (JsonPatchException e) {
            throw new IllegalArgumentException("Invalid patch");
        }

        return objectMapper.convertValue(patchedSubWarehouse, SubWarehouseResponseDto.class);
    }
}
