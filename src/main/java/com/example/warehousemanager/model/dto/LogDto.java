package com.example.warehousemanager.model.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.EAN;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LogDto {
    @Positive
    private Long quantity;
    @EAN
    private String productEan;
    @NotNull
    private Long warehouseFromId;
    @NotNull
    private Long warehouseToId;
    private Long subWarehouseFromId;
    private Long subWarehouseToId;
}
