package com.example.warehousemanager.model.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubWarehouseDto {
    private String name;
    @NotBlank
    private String address;
    private Long warehouseId;
}
