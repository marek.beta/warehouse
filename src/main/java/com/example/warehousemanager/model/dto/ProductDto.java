package com.example.warehousemanager.model.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.EAN;

@Data
@Builder
public class ProductDto {
    @EAN
    private String ean;
    @NotBlank
    private String name;
}
