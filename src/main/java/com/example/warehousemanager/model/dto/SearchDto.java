package com.example.warehousemanager.model.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SearchDto {
    LocalDateTime from;
    LocalDateTime to;
    String productEan;
}
