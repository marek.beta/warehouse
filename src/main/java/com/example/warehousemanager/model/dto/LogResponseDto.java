package com.example.warehousemanager.model.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.EAN;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LogResponseDto {
    private Long id;
    @Positive
    private Long quantity;
    @EAN
    private String productEan;
    @NotNull
    private Long warehouseFromId;
    @NotNull
    private Long warehouseToId;
    private Long subWarehouseFromId;
    private Long subWarehouseToId;
    private LocalDateTime timestamp;
}
