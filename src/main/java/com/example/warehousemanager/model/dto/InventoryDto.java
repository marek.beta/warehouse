package com.example.warehousemanager.model.dto;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.EAN;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InventoryDto {
    @Positive
    private Long quantity;
    @NotBlank
    private String serialNumber;
    @FutureOrPresent
    private LocalDate expirationDate;
    @EAN
    private String productEan;
    @NotNull
    private Long operationTypeId;
    @NotNull
    private Long warehouseId;
    private Long subWarehouseId;
}
