package com.example.warehousemanager.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "log")
public class LogEntity {

    @PrePersist
    public void prePersist() {
        timestamp = LocalDateTime.now();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long quantity;
    @ManyToOne
    @JoinColumn(name = "product_ean")
    private ProductEntity product;
    @ManyToOne
    @JoinColumn(name = "warehouse_from_id")
    private WarehouseEntity warehouseFrom;
    @ManyToOne
    @JoinColumn(name = "warehouse_to_id")
    private WarehouseEntity warehouseTo;
    @ManyToOne
    @JoinColumn(name = "subwarehouse_from_id")
    private SubWarehouseEntity subWarehouseFrom;
    @ManyToOne
    @JoinColumn(name = "subwarehouse_to_id")
    private SubWarehouseEntity subWarehouseTo;
    private LocalDateTime timestamp;
}
