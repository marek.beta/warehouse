package com.example.warehousemanager.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "inventory")
public class InventoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long quantity;
    @ManyToOne
    @JoinColumn(name = "product_ean")
    private ProductEntity product;
    @ManyToOne
    @JoinColumn(name = "operation_type_id")
    private OperationTypeEntity operationType;
    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private WarehouseEntity warehouse;
    @ManyToOne
    @JoinColumn(name = "sub_warehouse_id")
    private SubWarehouseEntity subWarehouse;
    private LocalDate expirationDate;
    private String serialNumber;
}
