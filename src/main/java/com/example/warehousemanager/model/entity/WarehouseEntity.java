package com.example.warehousemanager.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "warehouse")
public class WarehouseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    @OneToMany(mappedBy = "warehouse")
    private List<SubWarehouseEntity> subWarehouses = new ArrayList<>();

    public void addSubWarehouse(SubWarehouseEntity subWarehouseEntity) {
        subWarehouses.add(subWarehouseEntity);
    }
}
