package com.example.warehousemanager.controller;

import com.example.warehousemanager.exception.OperationTypeNotFoundException;
import com.example.warehousemanager.model.dto.OperationTypeDto;
import com.example.warehousemanager.model.dto.OperationTypeResponseDto;
import com.example.warehousemanager.service.OperationTypeService;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/operation-types")
public class OperationTypeController {
    private final OperationTypeService operationTypeService;

    @GetMapping
    public ResponseEntity<Page<OperationTypeResponseDto>> getAllOperationTypes(Pageable pageable) {
        Page<OperationTypeResponseDto> operationTypes = operationTypeService.findAll(pageable);
        return new ResponseEntity<>(operationTypes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OperationTypeResponseDto> getOperationTypeById(@PathVariable("id") Long id) throws OperationTypeNotFoundException {
        OperationTypeResponseDto operationTypeDto = operationTypeService.findOperationTypeById(id);
        return new ResponseEntity<>(operationTypeDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OperationTypeResponseDto> createOperationType(@Valid @RequestBody OperationTypeDto operationTypeDto) {
        OperationTypeResponseDto operationTypeResponse = operationTypeService.createOperationType(operationTypeDto);
        return new ResponseEntity<>(operationTypeResponse, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<OperationTypeResponseDto> updateOperationType(@PathVariable("id") Long id, @RequestBody JsonMergePatch operationTypePatch) throws OperationTypeNotFoundException {
        OperationTypeResponseDto updatedOperationType = operationTypeService.updateOperationType(id, operationTypePatch);
        return new ResponseEntity<>(updatedOperationType, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOperationType(@PathVariable("id") Long id) {
        operationTypeService.deleteOperationTypeById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
