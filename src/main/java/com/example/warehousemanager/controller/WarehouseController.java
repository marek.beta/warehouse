package com.example.warehousemanager.controller;

import com.example.warehousemanager.exception.WarehouseNotFoundException;
import com.example.warehousemanager.model.dto.WarehouseDto;
import com.example.warehousemanager.model.dto.WarehouseResponseDto;
import com.example.warehousemanager.service.WarehouseService;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/warehouses")
public class WarehouseController {
    private final WarehouseService warehouseService;

    // create mapping for attaching subwarehouse to warehouse
    @GetMapping("/{id}/attach/{subWarehouseId}")
    public ResponseEntity<WarehouseResponseDto> attachSubWarehouseToWarehouse(@PathVariable("id") Long id, @PathVariable("subWarehouseId") Long subWarehouseId) {
        WarehouseResponseDto warehouseResponseDto = warehouseService.attachSubWarehouseToWarehouse(id, subWarehouseId);
        return new ResponseEntity<>(warehouseResponseDto, HttpStatus.OK);
    }


    @GetMapping
    public ResponseEntity<Page<WarehouseResponseDto>> getAllWarehouses(Pageable pageable) {
        Page<WarehouseResponseDto> warehouses = warehouseService.findAll(pageable);
        return new ResponseEntity<>(warehouses, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<WarehouseResponseDto> getWarehouseById(@PathVariable("id") Long id) throws WarehouseNotFoundException {
        WarehouseResponseDto warehouseResponseDto = warehouseService.findWarehouseById(id);
        return new ResponseEntity<>(warehouseResponseDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<WarehouseResponseDto> createWarehouse(@Valid @RequestBody WarehouseDto warehouseDto) {
        WarehouseResponseDto warehouseResponseDto = warehouseService.createWarehouse(warehouseDto);
        return new ResponseEntity<>(warehouseResponseDto, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<WarehouseResponseDto> updateWarehouse(@PathVariable("id") Long id, @RequestBody JsonMergePatch patch) throws WarehouseNotFoundException {
        WarehouseResponseDto updatedWarehouse = warehouseService.updateWarehouse(id, patch);
        return new ResponseEntity<>(updatedWarehouse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteWarehouse(@PathVariable("id") Long id) {
        warehouseService.deleteWarehouseById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
