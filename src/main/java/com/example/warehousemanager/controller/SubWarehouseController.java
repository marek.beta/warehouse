package com.example.warehousemanager.controller;

import com.example.warehousemanager.exception.SubWarehouseNotFoundException;
import com.example.warehousemanager.model.dto.SubWarehouseDto;
import com.example.warehousemanager.model.dto.SubWarehouseResponseDto;
import com.example.warehousemanager.service.SubWarehouseService;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/subwarehouses")
public class SubWarehouseController {
    private final SubWarehouseService subWarehouseService;

    @GetMapping
    public ResponseEntity<Page<SubWarehouseResponseDto>> getAllSubWarehouses(Pageable pageable) {
        Page<SubWarehouseResponseDto> subWarehouses = subWarehouseService.findAll(pageable);
        return new ResponseEntity<>(subWarehouses, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SubWarehouseResponseDto> getSubWarehouseById(@PathVariable("id") Long id) throws SubWarehouseNotFoundException {
        SubWarehouseResponseDto subWarehouseResponseDto = subWarehouseService.findSubWarehouseById(id);
        return new ResponseEntity<>(subWarehouseResponseDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SubWarehouseResponseDto> createSubWarehouse(@Valid @RequestBody SubWarehouseDto subWarehouseDto) {
        SubWarehouseResponseDto createdSubWarehouse = subWarehouseService.createSubWarehouse(subWarehouseDto);
        return new ResponseEntity<>(createdSubWarehouse, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<SubWarehouseResponseDto> updateSubWarehouse(@PathVariable("id") Long id, @RequestBody JsonMergePatch patch) throws SubWarehouseNotFoundException {
        SubWarehouseResponseDto updatedSubWarehouse = subWarehouseService.updateSubWarehouse(id, patch);
        return new ResponseEntity<>(updatedSubWarehouse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteSubWarehouse(@PathVariable("id") Long id) {
        subWarehouseService.deleteSubWarehouseById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
