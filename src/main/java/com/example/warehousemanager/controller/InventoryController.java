package com.example.warehousemanager.controller;

import com.example.warehousemanager.exception.InventoryNotFoundException;
import com.example.warehousemanager.model.dto.InventoryDto;
import com.example.warehousemanager.model.dto.InventoryResponseDto;
import com.example.warehousemanager.service.InventoryService;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/inventories")
public class InventoryController {
    private final InventoryService inventoryService;

    @GetMapping
    public ResponseEntity<Page<InventoryResponseDto>> getAllInventories(Pageable pageable) {
        Page<InventoryResponseDto> inventories = inventoryService.findAll(pageable);
        return new ResponseEntity<>(inventories, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<InventoryResponseDto> getInventoryById(@PathVariable("id") Long id) throws InventoryNotFoundException {
        InventoryResponseDto inventoryResponseDto = inventoryService.findInventoryById(id);
        return new ResponseEntity<>(inventoryResponseDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<InventoryResponseDto> createInventory(@Valid @RequestBody InventoryDto inventoryDto) {
        InventoryResponseDto inventoryResponseDtoResponse = inventoryService.createInventory(inventoryDto);
        return new ResponseEntity<>(inventoryResponseDtoResponse, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<InventoryResponseDto> updateInventory(@PathVariable("id") Long id, @RequestBody JsonMergePatch patch) throws InventoryNotFoundException {
        InventoryResponseDto updatedInventory = inventoryService.updateInventory(id, patch);
        return new ResponseEntity<>(updatedInventory, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteInventory(@PathVariable("id") Long id) {
        inventoryService.deleteInventory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
