package com.example.warehousemanager.controller;

import com.example.warehousemanager.exception.LogNotFoundException;
import com.example.warehousemanager.model.dto.LogDto;
import com.example.warehousemanager.model.dto.LogResponseDto;
import com.example.warehousemanager.model.dto.SearchDto;
import com.example.warehousemanager.service.LogService;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/logs")
public class LogController {
    private final LogService logService;

    @GetMapping("/search")
    public ResponseEntity<Page<LogResponseDto>> searchLogs(@RequestBody SearchDto searchDto, Pageable pageable) {
        Page<LogResponseDto> logs;
        if (searchDto.getProductEan() != null) {
            logs = logService.searchLogs(searchDto.getProductEan(), searchDto.getFrom(), searchDto.getTo(), pageable);
        } else if (searchDto.getFrom() != null && searchDto.getTo() != null) {
            logs = logService.searchLogs(searchDto.getFrom(), searchDto.getTo(), pageable);
        } else {
            logs = logService.findAll(pageable);
        }

        return new ResponseEntity<>(logs, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Page<LogResponseDto>> getAllLogs(Pageable pageable) {
        Page<LogResponseDto> logs = logService.findAll(pageable);
        return new ResponseEntity<>(logs, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LogResponseDto> getLogById(@PathVariable("id") Long id) throws LogNotFoundException {
        LogResponseDto logResponseDto = logService.findLogById(id);
        return new ResponseEntity<>(logResponseDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<LogResponseDto> createLog(@Valid @RequestBody LogDto logDto) {
        LogResponseDto logResponseDto = logService.createLog(logDto);
        return new ResponseEntity<>(logResponseDto, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<LogResponseDto> updateLog(@PathVariable("id") Long id, @RequestBody JsonMergePatch logPatch) throws LogNotFoundException {
        LogResponseDto updatedLog = logService.updateLog(id, logPatch);
        return new ResponseEntity<>(updatedLog, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteLog(@PathVariable("id") Long id) {
        logService.deleteLogById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
