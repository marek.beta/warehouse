package com.example.warehousemanager.controller;

import com.example.warehousemanager.exception.ProductNotFoundException;
import com.example.warehousemanager.model.dto.ProductDto;
import com.example.warehousemanager.service.ProductService;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public ResponseEntity<Page<ProductDto>> getAllProducts(Pageable pageable) {
        Page<ProductDto> products = productService.findAll(pageable);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/{ean}")
    public ResponseEntity<ProductDto> getProductByEan(@PathVariable("ean") String ean) throws ProductNotFoundException {
        ProductDto productDto = productService.findProductByEan(ean);
        return new ResponseEntity<>(productDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ProductDto> createProduct(@Valid @RequestBody ProductDto productDto) {
        ProductDto createdProductDto = productService.createProduct(productDto);
        return new ResponseEntity<>(createdProductDto, HttpStatus.CREATED);
    }

    @PatchMapping("/{ean}")
    public ResponseEntity<ProductDto> updateProduct(@PathVariable("ean") String ean, @RequestBody JsonMergePatch patch) throws ProductNotFoundException {
        ProductDto updatedProduct = productService.updateProduct(ean, patch);
        return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
    }

    @DeleteMapping("/{ean}")
    public ResponseEntity deleteProduct(@PathVariable("ean") String ean) {
        productService.deleteProductByEan(ean);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
